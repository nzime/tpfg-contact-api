<?php

namespace futureactivities\contactapi\migrations;

use Craft;
use craft\db\Migration;

/**
 * m220125_151250_result migration.
 */
class m220125_151250_result extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if ($this->db->tableExists('{{%contact_messages}}') && !$this->db->columnExists('{{%contact_messages}}', 'result')) {
            $this->addColumn('{{%contact_messages}}', 'result', $this->text()->after('processed')->null());
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m220125_151250_result cannot be reverted.\n";
        return false;
    }
}
